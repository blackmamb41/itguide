import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity , AsyncStorage} from 'react-native';
import { Container, Content, Footer, FooterTab, Button, Header, Icon, DeckSwiper } from 'native-base';
import { createStackNavigator, createAppContainer, StackActions, NavigationActions } from 'react-navigation';

export default class Contact extends React.Component {
    static navigationOptions = {
      title: 'FAQ',
      headerStyle: {
        backgroundColor: '#FF0000',
      },
      headerTitleStyle: {
        color: '#fff',
        textAlign: 'center',
        flex: 1,
      }
    }

    profileCondition = () => {
      AsyncStorage.getItem('token', (error, data) => {
        if(data == null) {
          this.props.navigation.dispatch(StackActions.reset({index: 0, actions: [NavigationActions.navigate({routeName: 'Login'})]}))
        } else {
          this.props.navigation.dispatch(StackActions.reset({index: 0, actions: [NavigationActions.navigate({routeName: 'Profile'})]}))
        }
      })
    }
  
    HistoryCondition = () => {
      AsyncStorage.getItem('token', (error, data) => {
        if(data == null) {
          this.props.navigation.dispatch(StackActions.reset({index: 0, actions: [NavigationActions.navigate({routeName: 'Login'})]}))
        } else {
          this.props.navigation.dispatch(StackActions.reset({index: 0, actions: [NavigationActions.navigate({routeName: 'History'})]}))
        }
      })
    }
  
    render() {
      return (
        <Container>
          <Content>
            <Text>Ini Contact Screen</Text>
          </Content>
          <Footer>
          <FooterTab style={{backgroundColor: '#ff0000'}}>
            <Button style={{backgroundColor: '#ff0000'}} onPress={() => {
              this.props.navigation.dispatch(StackActions.reset({
                index: 0,
                actions: [
                  NavigationActions.navigate({routeName: 'Home'})
                ],
              }))
            }}>
              <Icon name="home" style={{color: '#fff'}}/>
            </Button>
            <Button title="Go To FAQ" style={{backgroundColor: '#ffAAAA'}} active>
              <Icon name="help" style={{color: '#000'}}/>
            </Button>
            <Button title="Contact" onPress={() => {
              this.props.navigation.dispatch(StackActions.reset({
                index: 0,
                actions: [
                  NavigationActions.navigate({routeName: 'Contact'})
                ],
              }))
            }}>
              <Icon name="call" style={{color: '#fff'}} />
            </Button>
            <Button onPress={this.HistoryCondition}>
              <Icon name='list' style={{color: '#fff'}} />
            </Button>
            <Button onPress={this.profileCondition}>
              <Icon name='person' style={{color: '#fff'}} />
            </Button>
          </FooterTab>
        </Footer>
        </Container>
      )
    }
  }