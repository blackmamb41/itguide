import React from 'react'
import { 
    Text, 
    View,
    AsyncStorage,
    ImageBackground,
    StyleSheet 
} from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import { Container } from 'native-base';

const styling = StyleSheet.create({
    bgImage: {flex: 1, width: '100%', height: '100%'}
})

export default class Splash extends React.Component {
    static navigationOptions = {
        mode: 'modal',
        header: null,
        headerMode: 'none',
    }

    async componentDidMount() {
        AsyncStorage.getItem('nik', (error, data) => {
            console.log(data)
            if(data == null) {
                this.props.navigation.dispatch(StackActions.reset({index: 0, actions: [NavigationActions.navigate({routeName: 'Login'})]}))
            } else {
                this.props.navigation.dispatch(StackActions.reset({index: 0, actions: [NavigationActions.navigate({routeName: 'Home'})]}))
            }
        })
    }

    render() {
        return (
            <Container>
                <ImageBackground style={styling.bgImage} source={require('./assets/wallpaper.png')}>
                </ImageBackground>
            </Container>
        )
    }
}