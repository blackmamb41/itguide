import axios from "axios"
import { StackActions, NavigationActions } from "react-navigation";

const goToBookingCalendar = () => {
    this.props.navigation.dispatch(StackActions.reset({
        index: 0,
        actions: [
            NavigationActions.navigate({routeName: 'Booking'})
        ]
    }))
}

export { goToBookingCalendar }