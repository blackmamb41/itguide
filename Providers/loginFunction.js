import axios from "axios";

const Login = (credentials) => {
    axios.post('/action/login', {
        nik: credentials.nik,
        password: credentials.password
    }).then(function(response) {
        alert(response)
    }).catch(function(err) {
        alert(err)
    });
}

export { Login }