import React from 'react'
import { View, Text, AsyncStorage, Image } from 'react-native';
import { Button, Container, Content, Footer, FooterTab, Icon, Item, Label, Input } from 'native-base';
import { StackActions, NavigationActions } from 'react-navigation';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Axios from 'axios';
import { urlApi } from './utils';

// const dataProfile = AsyncStorage.getItem('nik');

export default class Profile extends React.Component {
    static navigationOptions = {
        title: 'Profile',
        header: null,
        headerMode: 'none',
    }

    constructor(props) {
        super(props)
        this.state = {
            nik: null,
            name: null,
            position: null,
            division: null,
            photo: './assets/user.png',
            data: {},
        }
    }

    async componentDidMount() {
        await AsyncStorage.getItem('nik', (error, data) => {
            this.setState({nik: data})
        })
        this.getEmployee(this.state.nik)
            .then(eData => {
                let employeeData = {
                    name: eData.name,
                    division: eData.divison,
                    position: eData.position,
                    photo: eData.photo,
                    expired: eData.expired
                }
                // AsyncStorage.setItem('dataEmployee', JSON.stringify(employeeData))
                console.log('Success')
            }).catch(error => console.log(error))
        // console.log(this.state.data)
        await AsyncStorage.getItem('dataEmployee', (error, item) => {
            this.setState({name: JSON.parse(item).name})
            this.setState({data: JSON.parse(item)})
        })
        console.log(this.state.data)
    }

    getEmployee(nik) {
        return Axios.get(urlApi+'/employee/'+nik)
            .then(res => {
                return res.data
            })
    }

    getObjectDataEmployee() {
        AsyncStorage.getItem('dataEmployee', (error, data) => {
            let jsonDataEmployee = JSON.parse(data)
            this.setState({data: jsonDataEmployee})
        })
        return this.state.data
    }

    logoutAccount = () => {
        AsyncStorage.removeItem('token');
        AsyncStorage.removeItem('nik')
        AsyncStorage.removeItem('dataEmployee')
        this.props.navigation.dispatch(StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({routeName: 'Home'})
            ],
        }))
    }

    HistoryCondition = () => {
        AsyncStorage.getItem('token', (error, data) => {
          if(data == null) {
            this.props.navigation.dispatch(StackActions.reset({index: 0, actions: [NavigationActions.navigate({routeName: 'Login'})]}))
          } else {
            this.props.navigation.dispatch(StackActions.reset({index: 0, actions: [NavigationActions.navigate({routeName: 'History'})]}))
          }
        })
    }

    render() {
        return(
            <Container style={{flex: 1, justifyContent: 'center'}}>
                <Content>
                    <Grid>
                        <Row style={{backgroundColor: '#ff0000'}}>
                            <Col style={{flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 100, marginBottom: 100}}>
                                <Image style={{borderWidth: 2, borderColor: '#fff', borderRadius: 50, backgroundColor: '#fff'}} source={require('./assets/user.png')} />
                            </Col>
                        </Row>
                        <Row style={{marginTop: 5, marginBottom: 5, marginLeft: 5}}>
                            <Col>
                                <Item stackedLabel disabled>
                                    <Label style={{color: '#0A60FF'}}>Name</Label>
                                    <Input value={this.state.data.name} />
                                </Item>
                            </Col>
                        </Row>
                        <Row style={{marginTop: 5, marginBottom: 5, marginLeft: 5}}>
                            <Col>
                                <Item stackedLabel disabled>
                                    <Label style={{color: '#0A60FF'}}>NIK</Label>
                                    <Input value={this.state.nik} />
                                </Item>
                            </Col>
                        </Row>
                        <Row style={{marginTop: 5, marginBottom: 5, marginLeft: 5}}>
                            <Col>
                                <Item stackedLabel disabled>
                                    <Label style={{color: '#0A60FF'}}>Position</Label>
                                    <Input value={this.state.data.division + '-' + this.state.data.position} />
                                </Item>
                            </Col>
                        </Row>
                        <Row style={{marginTop: 5, marginBottom: 5, marginLeft: 5}}>
                            <Col>
                                <Item stackedLabel disabled>
                                    <Label style={{color: '#0A60FF'}}>No. Handphone</Label>
                                    <Input value='628888888' />
                                </Item>
                            </Col>
                        </Row>
                        <Row style={{marginTop: 40, marginBottom: 5, marginLeft: 5, flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                            <Col>
                                <Button style={{backgroundColor: '#ff0000', width: '50%', paddingRight: 50, paddingLeft: 50}} onPress={this.logoutAccount}><Text style={{color: '#fff'}}>LOGOUT</Text></Button> 
                            </Col>
                        </Row>
                    </Grid>
                </Content>
                <Footer>
                    <FooterTab style={{backgroundColor: '#ff0000'}}>
                        <Button title="Home" onPress={() => {
                            this.props.navigation.dispatch(StackActions.reset({
                                index: 0,
                                actions: [
                                    NavigationActions.navigate({routeName: 'Home'})
                                ],
                                }))
                            }}>
                            <Icon name="home" style={{color: '#fff'}}/>
                        </Button>
                        <Button title="Go To FAQ" onPress={() => {
                            this.props.navigation.dispatch(StackActions.reset({
                                index: 0,
                                actions: [
                                    NavigationActions.navigate({routeName: 'FAQ'})
                                ],
                                }))
                            }}>
                            <Icon name="help" style={{color: '#fff'}}/>
                        </Button>
                        <Button title="Contact" onPress={() => {
                            this.props.navigation.dispatch(StackActions.reset({
                                index: 0,
                                actions: [
                                NavigationActions.navigate({routeName: 'Contact'})
                                ],
                                }))
                            }}>
                            <Icon name="call" style={{color: '#fff'}} />
                        </Button>
                        <Button onPress={this.HistoryCondition}>
                            <Icon name='list' style={{color: '#fff'}} />
                        </Button>
                        <Button style={{backgroundColor: '#FFAAAA'}} title="Profile" active>
                            <Icon name='person' style={{color: '#000'}} />
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        )
    }
}