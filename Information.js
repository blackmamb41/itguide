import React from 'react'
import { 
    View,
    Text,
    Image
} from 'react-native';
import { 
    StackActions, 
    NavigationActions 
} from 'react-navigation';
import { 
    Container, 
    Content, 
    Card, 
    CardItem, 
    Icon, 
    Right,
    Thumbnail, 
    Spinner,
    Body
} from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Axios from 'axios';
import { urlApi } from './utils';
import { unserialize } from 'locutus/php/var';

export default class Information extends React.Component {
    static navigationOptions = {
        title: 'Information',
        headerStyle: {
          backgroundColor: '#FF0000',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          color: '#fff',
          textAlign: 'center',
          alignSelf: 'center'
        }
    }

    constructor(props) {
        super(props)
        this.state = {
            today: [],
            tomorrow: [],
            afterTomorrow: [],
            loading: false
        }
    }

    componentWillMount() {
        this.setState({loading: !this.state.loading})
        Axios.get(urlApi+'/information/highlight')
            .then(response => {
                this.setState({
                    today: response.data.today,
                    tomorrow: response.data.tomorrow,
                    afterTomorrow: response.data.aftertomorrow,
                    loading: !this.state.loading
                })
            }).catch(error => {
                console.log(error)
                this.setState({loading: !this.state.loading})
            })
    }

    getDateNow(date) {
        var now = new Date(date),
            month = '' + (now.getMonth() + 1),
            day = '' + (now.getDate() + 2),
            year = now.getFullYear();
        if (month.length < 2) month = '0' + month
        if (day.length < 2) day = '0' + day

        return [year, month, day].join('/');
    }

    todayRender() {
        const todayMap = this.state.today.map((item) => {
            const jam = unserialize(item.jam).toString()
            return (
                <CardItem>
                    <Thumbnail style={{width: 32, height: 32}} source={require('./assets/skyline.png')} />
                    <Text style={{marginLeft: '10%'}}>{item.name}</Text>
                    <Right>
                        <Text><Icon style={{color: '#000'}} name='time' /> : {jam}</Text>
                    </Right>
                </CardItem>
            )
        })
        if(this.state.loading == true) {
            return (
                <CardItem>
                    <Spinner color='green' />
                </CardItem>
            )
        } else if(this.state.today.length == 0) {
            return (
                <CardItem>
                    <Body>
                        <Text>NO SCHEDULE</Text>
                    </Body>
                </CardItem>
            )
        } else {
            return todayMap
        }
    }

    tomorrowRender() {
        const tomorrowMap = this.state.tomorrow.map((item) => {
            const jam = unserialize(item.jam).toString()
            return (
                <CardItem>
                    <Thumbnail style={{width: 32, height: 32}} source={require('./assets/skyline.png')} />
                    <Text style={{marginLeft: '10%'}}>{item.name}</Text>
                    <Right>
                        <Text><Icon style={{color: '#000'}} name='time' /> : {jam}</Text>
                    </Right>
                </CardItem>
            )
        })
        if(this.state.loading == true) {
            return (
                <CardItem>
                    <Spinner color='green' />
                </CardItem>
            )
        } else if(this.state.tomorrow.length == 0) {
            return (
                <CardItem>
                    <Body>
                        <Text>NO SCHEDULE</Text>
                    </Body>
                </CardItem>
            )
        } else {
            return tomorrowMap
        }
    }

    renderAfterTomorrow() {
        const lusa = this.state.afterTomorrow
        const lusaMap = lusa.map((item) => {
            const jam = unserialize(item.jam).toString()
            return (
                <CardItem>
                    <Thumbnail style={{width: 32, height: 32}} source={require('./assets/skyline.png')} />
                    <Text style={{marginLeft: '10%'}}>{item.name}</Text>
                    <Right>
                        <Text><Icon style={{color: '#000'}} name='time' /> : {jam}</Text>
                    </Right>
                </CardItem>
            )
        })
        if(this.state.loading == true) {
            return (
                <CardItem>
                    <Spinner color='green' />
                </CardItem>
            )
        } else if(this.state.afterTomorrow.length == 0) {
            return (
                <CardItem>
                    <Body>
                        <Text>NO SCHEDULE</Text>
                    </Body>
                </CardItem>
            )
        } else {
            return lusaMap
        }
    }

    render() {
        return (
            <Container style={{backgroundColor: '#eee'}}>
                <Content>
                    <Text style={{fontSize: 30, margin: 10}}>TODAY</Text>
                    <Card>
                        {this.todayRender()}
                    </Card>
                    <Text style={{fontSize: 30, margin: 10}}>TOMORROW</Text>
                    <Card>
                        {this.tomorrowRender()}
                    </Card>
                    <Text style={{fontSize: 30, margin: 10}}>{this.getDateNow(Date.now())}</Text>
                    <Card>
                        {this.renderAfterTomorrow()}
                    </Card>
                </Content>
            </Container>
        )
    }
}