import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { createStackNavigator, createAppContainer, StackActions, NavigationActions } from 'react-navigation';
import Avaya from './Avaya';
import HomeScreen from './HomeScreen';
import FAQ from './FAQ';
import Contact from './Contact';
import Booking from './Booking';
import Login from './Login';
import Profile from './Profile';
import History from './History';
import Tiket from './Tiket';
import newBooking from './newBooking';
import successBook from './successBook';
import Tutorial from './Tutorial';
import Settings from './Settings';
import Splash from './Splash';
import Solotiket from './Solotiket';
import Information from './Information';

const Main = createStackNavigator({
  Splash: {
    screen: Splash
  },
  Home : {
    screen: HomeScreen,
  },
  FAQ: {
    screen: FAQ,
  },
  Contact: {
    screen: Contact,
  },
  AvayaTutorial: {
    screen: Avaya,
  },
  Booking: {
    screen: newBooking
  },
  Login: {
    screen: Login
  },
  Profile: {
    screen: Profile
  },
  History: {
    screen: History
  },
  Tiket: {
    screen: Tiket
  },
  successBook: {
    screen: successBook
  },
  Tutorial: {
    screen: Tutorial
  },
  Settings: {
    screen: Settings
  },
  Solotiket: {
    screen: Solotiket
  },
  Information: {
    screen: Information
  }
}, {
  initialRouteName: 'Splash',
});

const Layout = StyleSheet.create({
  mainMenu: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    margin: 20,
    paddingTop: 30,
    paddingBottom: 30,
    // paddingLeft: 50,
    // paddingRight: 50,
    borderWidth: 0.5,
    borderRadius: 9,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 9,
    elevation: 3,
  },
  titleMenu: {
    textAlign: 'center',
    fontSize: 10,
  },
  thumbMenu: {
    width: 40,
    height: 40,
  }
})

export default createAppContainer(Main);