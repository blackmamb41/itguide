import React from 'react'
import {
    AsyncStorage,
    Text,
} from 'react-native'
import { 
    Container, 
    Content, 
    Footer, 
    FooterTab, 
    Button, 
    Header, 
    Icon, 
    Badge,
    Input,
    Form,
    Textarea,
    View,
    Spinner
} from 'native-base';
import { 
    createStackNavigator, 
    createAppContainer, 
    StackActions, 
    NavigationActions 
} from 'react-navigation';
import { 
    Col, 
    Grid, 
    Row 
} from 'react-native-easy-grid';
import DatePicker from 'react-native-datepicker'
import Axios from 'axios';
import { urlApi } from './utils';
import { unserialize } from 'locutus/php/var';
import SelectMultipleGroupButton from 'react-native-selectmultiple-button/libraries/SelectMultipleGroupButton';
import Toast from 'react-native-easy-toast';

export default class newBooking extends React.Component {
    static navigationOptions = {
        title: 'Booking',
        headerStyle: {
          backgroundColor: '#FF0000',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          color: '#fff',
          textAlign: 'center',
          alignSelf: 'center'
        }
    }

    constructor(props) {
        super(props)
        this.state = {
            tanggal: null,
            loading: true,
            intialClock: [
                { jam: 6, status: 'empty'},
                { jam: 7, status: 'empty'},
                { jam: 8, status: 'empty'},
                { jam: 9, status: 'empty'},
                { jam: 10, status: 'empty'},
                { jam: 11, status: 'empty'},
                { jam: 12, status: 'empty'},
                { jam: 13, status: 'empty'},
                { jam: 14, status: 'empty'},
                { jam: 15, status: 'empty'},
                { jam: 16, status: 'empty'},
                { jam: 17, status: 'empty'},
                { jam: 18, status: 'empty'},
            ],
            dataJam: [],
            modalCreateVisible: false,
            modalGetSingleVisible: false,
            pilihJam: [],
            perihal: null,
            dataEmployee: {},
            nikEmployee: null,
            singleData: {},
            loading: false
        }
        this.setDate = this.setDate.bind(this);
    }

    componentDidMount() {
        AsyncStorage.getItem('nik', (error, data) => {
          this.setState({nikEmployee: data})
        })
        AsyncStorage.getItem('dataEmployee', (error, data) => {
            this.setState({dataEmployee: JSON.parse(data)})
        })
    }

    setDate(tglDipilih) {
        // console.log(this.state.dataEmployee)
        this._deniedSelectJam()
        this.setState({
            tanggal: tglDipilih,
            loading: !this.state.loading
        })
        this.getDataCheckBooking()
    }

    getTimeNow(date) {
        let now = new Date(date),
            hh = now.getHours(),
            mm = now.getMinutes(),
            ss = now.getSeconds();
        if(hh.length < 2) hh = '0' + hh
        if(mm.length < 2) mm = '0' + mm
        if(ss.length < 2) ss = '0' + ss

        return [hh,mm,ss].join(':')
    }

    getDateNow(date) {
        var now = new Date(date),
            month = '' + (now.getMonth() + 1),
            day = '' + (now.getDate()),
            year = now.getFullYear();
        if (month.length < 2) month = '0' + month
        if (day.length < 2) day = '0' + day

        return [year, month, day].join('-');
    }

    getDataCheckBooking = () => {
        Axios.get(urlApi+'/booking/check/tanggal/'+this.state.tanggal)
            .then(response => {
                this.setState({dataJam: response.data.data})
                this.setState({loading: !this.state.loading})
                this.renderJamButton()
            }).catch(error => {
                console.log(error)
            })
    }

    _deniedSelectJam() {
        this.state.pilihJam.length = 0
    }

    _onChangeSelectJam(selectedJam) {
        this.setState({
            pilihJam: selectedJam
        })
    }

    _bookNow = () => {
        if(this.state.pilihJam.length == 0) {
            this.refs.peringatan.show(<View><Text style={{color: '#fff', justifyContent: 'center', alignItems: 'center'}}>BOOKING DIBATALKAN{"\n"}{"\n"}PILIH JAM MEETING DAHULU</Text></View>)
        } 
        else if(this.state.perihal == null) {
            this.refs.peringatan.show(<View><Text style={{color: '#fff', justifyContent: 'center', alignItems: 'center'}}>BOOKING DIBATALKAN{"\n"}{"\n"}ISI DAHULU PERIHAL MEETING</Text></View>)
        } else {
            Axios.post(urlApi+'/booking/add', {
                perihal: this.state.perihal,
                tanggal: this.state.tanggal,
                jam: this.state.pilihJam,
                employees_nik: this.state.nikEmployee
            }).then(response => {
                this.createActivity()
                this.props.navigation.dispatch(StackActions.reset({index: 0, actions: [NavigationActions.navigate({routeName: 'successBook'})]}))
            }).catch(error => {
                console.log(error)
            })
        }
    }

    renderJamButton() {
        if(this.state.tanggal == null) {
            return(<Text>Silahkan Pilih Tanggal Dahulu</Text>)
        } else if(this.state.loading == true) {
            return(
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                    <Spinner color='green' />
                </View>
            )
        }
        else {
            const jam = [8,9,10,11,12,13,14,15,16,17]
            let margeJamBooked = []
            const BookedJam = this.state.dataJam.map((bookingJam) => {
                unserialize(bookingJam.jam).map(datajam => {
                    margeJamBooked.push(datajam)
                })
            })
            const jamEx = jam.filter((list) => {
                return !margeJamBooked.includes(list)
            })
            jamKosong = []
            jamEx.map((data) => {
                jamKosong.push({value: data, displayValue: data})
            })
            console.log(this.state.pilihJam)
            return(
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{fontWeight: 'bold', fontSize: 20, margin: 10}}>AVAILABLE CLOCK</Text>
                    <SelectMultipleGroupButton
                        containerViewStyle={{ flexDirection: "row" }}
                        highLightStyle={{
                            borderColor: "gray",
                            backgroundColor: "transparent",
                            textColor: "gray",
                            borderTintColor: "green",
                            backgroundTintColor: "green",
                            textTintColor: "white"
                        }}
                        buttonViewStyle={{ width: 60, height: 60, borderRadius: 30 }}
                        group={jamKosong}
                        onSelectedValuesChange={selectedJam => this._onChangeSelectJam(selectedJam)}
                    />
                    <Text style={{fontWeight: 'bold', fontSize: 15, marginTop: 30, marginBottom: 5}}>KEPENTINGAN</Text>
                    <Form style={{width: '100%', marginTop: 20, borderRadius: 5, elevation: 1,}}>
                        <Textarea rowSpan={5} bordered onChangeText={(dataPerihal) => this.setState({perihal: dataPerihal})}/>
                    </Form>
                    <Button success style={{marginTop: 20, width: '100%', flex: 1, justifyContent: 'center', alignItems: 'center'}} onPress={this._bookNow}><Text style={{justifyContent: 'center', alignItems: 'center', color: '#fff'}}>Book Now</Text></Button>
                </View>
            )
        }
    }

    createActivity() {
        Axios.post(urlApi+'/activity/'+this.state.nikEmployee, {
            activity: 'Booking',
            tanggal: this.getDateNow(Date.now()),
            time: this.getTimeNow(Date.now()),
            ip: this.state.dataEmployee.ip,
            employees_nik: this.state.dataEmployee.nikEmployee
        }).then(response => {
            console.log(response.data)
        }).catch(error => {
            console.log(error)
        })
    }

    render() {
        return (
            <Container style={{flex: 1, justifyContent: 'center'}}>
                <Toast
                    ref='peringatan'
                    sstyle={{backgroundColor: '#ff0000', paddingTop: 150, paddingBottom: 150}}
                    position='top'
                    fadeOutDuration={3000}
                />
                <Content>
                    <Grid>
                    <Row style={{margin: 20, marginBottom: 30, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                        <DatePicker
                            style={{width: 200}}
                            date={this.state.tanggal} 
                            mode='date'
                            placeholder='Pilih Tanggal'
                            format='YYYY-MM-DD'
                            minDate={this.getDateNow(Date.now)}
                            maxDate='2099-10-10'
                            confirmBtnText='Pilih'
                            onDateChange={this.setDate}
                            customStyles={{
                                dateInput: {
                                    borderRadius: 5,
                                },
                            }}
                        />
                    </Row>
                    <Row style={{flex: 1, alignItems: 'center', justifyContent: 'center', margin: 10}}>
                        {this.renderJamButton()}
                    </Row>
                    </Grid>
                </Content>
            </Container>
        )
    }
}