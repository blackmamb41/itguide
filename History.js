import React from 'react';
import { Text, View, AsyncStorage, TouchableOpacity } from 'react-native';
import { 
    Container, 
    Header, 
    Content, 
    List, 
    ListItem,
    Left, 
    Body, 
    Right, 
    Thumbnail, 
    Footer, 
    FooterTab, 
    Icon, 
    Button,
    Card,
    CardItem,
    Badge
} from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { StackActions, NavigationActions } from 'react-navigation';
import Axios from 'axios';
import { urlApi } from './utils';

export default class History extends React.Component {
    static navigationOptions = {
        title: 'History',
        headerStyle: {
          backgroundColor: '#FF0000',
        },
        headerTitleStyle: {
          color: '#fff',
          textAlign: 'center',
          flex: 1,
        }
    }

    constructor(props) {
        super(props)
        this.state = {
            history: [],
            nik: null,
        }
    }

    async componentDidMount() {
        await AsyncStorage.getItem('nik', (error, data) => {
            this.setState({nik: data})
        })
        Axios.get(urlApi+'/employee/'+this.state.nik+'/history')
            .then(response => {
                this.setState({history: response.data})
            }).catch(error => {
                console.log(error)
            })
    }

    profileCondition = () => {
        AsyncStorage.getItem('token', (error, data) => {
          if(data == null) {
            this.props.navigation.dispatch(StackActions.reset({index: 0, actions: [NavigationActions.navigate({routeName: 'Login'})]}))
          } else {
            this.props.navigation.dispatch(StackActions.reset({index: 0, actions: [NavigationActions.navigate({routeName: 'Profile'})]}))
          }
        })
    }

    dataHistory = () => {
        realHistory = this.state.history
        const forHistory = realHistory.map((data) => {
            if(data.status == 'pending') {
                return (
                    <TouchableOpacity onPress={() => {
                        const dataItem = {
                            tiket: data.tiket,
                            jam: data.jam,
                            tanggal: data.tanggal
                        }
                        AsyncStorage.setItem('getHistoryItem', JSON.stringify(dataItem))
                        this.props.navigation.navigate('Tiket')
                    }}>
                        <Card>
                            <CardItem header bordered>
                                <Text>Tanggal : {data.tanggal}</Text>
                                <Right>
                                    <Text>Jam : {data.jam}</Text>
                                </Right>
                            </CardItem>
                            <CardItem bordered>
                                <Body>
                                    <Text>{data.perihal}</Text>
                                </Body>
                            </CardItem>
                            <CardItem footer bordered>
                                <Badge warning><Text style={{color: '#fff'}}>{data.status}</Text></Badge>
                            </CardItem>
                        </Card>
                    </TouchableOpacity>
                )
            } else if(data.status == 'checkin') {
                return (
                    <TouchableOpacity onPress={() => {
                        const dataItem = {
                            tiket: data.tiket,
                            jam: data.jam,
                            tanggal: data.tanggal
                        }
                        AsyncStorage.setItem('getHistoryItem', JSON.stringify(dataItem))
                        this.props.navigation.navigate('Tiket')
                    }}>
                        <Card>
                            <CardItem header bordered>
                                <Text>Tanggal : {data.tanggal}</Text>
                                <Right>
                                    <Text>Jam : {data.jam}</Text>
                                </Right>
                            </CardItem>
                            <CardItem bordered>
                                <Body>
                                    <Text>{data.perihal}</Text>
                                </Body>
                            </CardItem>
                            <CardItem footer bordered>
                                <Badge success><Text style={{color: '#fff'}}>{data.status}</Text></Badge>
                            </CardItem>
                        </Card>
                    </TouchableOpacity>
                )
            }
        })

        return forHistory
    }

    render() {
        return (
            <Container style={{flex: 1, justifyContent: 'center'}}>
                <Content>
                    <Grid>
                        <Row style={{marginLeft: 20, marginRight: 20}}>
                            <Col>
                                {this.dataHistory()}
                            </Col>
                        </Row>
                    </Grid>
                </Content>
                <Footer>
                    <FooterTab style={{backgroundColor: '#ff0000'}}>
                        <Button title="Home" onPress={() => {
                            this.props.navigation.dispatch(StackActions.reset({
                                index: 0,
                                actions: [
                                    NavigationActions.navigate({routeName: 'Home'})
                                ],
                                }))
                            }}>
                            <Icon name="home" style={{color: '#fff'}}/>
                        </Button>
                        <Button title="Go To FAQ" onPress={() => {
                            this.props.navigation.dispatch(StackActions.reset({
                                index: 0,
                                actions: [
                                    NavigationActions.navigate({routeName: 'FAQ'})
                                ],
                                }))
                            }}>
                            <Icon name="help" style={{color: '#fff'}}/>
                        </Button>
                        <Button title="Contact" onPress={() => {
                            this.props.navigation.dispatch(StackActions.reset({
                                index: 0,
                                actions: [
                                NavigationActions.navigate({routeName: 'Contact'})
                                ],
                                }))
                            }}>
                            <Icon name="call" style={{color: '#fff'}} />
                        </Button>
                        <Button style={{backgroundColor: '#FFAAAA'}} title="Profile" active>
                            <Icon name='list' style={{color: '#000'}} />
                        </Button>
                        <Button onPress={this.profileCondition}>
                            <Icon name='person' style={{color: '#fff'}} />
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        )
    }
}