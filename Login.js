import React from "react";
import { ImageBackground, Image, Text, StyleSheet, AsyncStorage, NetInfo } from "react-native";
import { Container, Header, Form, Icon, Item, Content, Input, View, Button, Spinner } from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";
import Axios from "axios";
import { urlApi } from "./utils";
import Toast, {DURATION} from 'react-native-easy-toast'
import { StackActions, NavigationActions } from 'react-navigation';
import Overlay from 'react-native-modal-overlay'
import { NetworkInfo } from 'react-native-network-info';
// import Profile from './Profile'

const styling = StyleSheet.create({
    bgImage: {flex: 1, width: '100%', height: '100%'},
    formContent: {flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center'},
    overlay: {...StyleSheet.absoluteFillObject, backgroundColor: 'rgba(0,0,0,0.4)'}
})
export default class Login extends React.Component {
    static navigationOptions = {
        mode: 'modal',
        header: null,
        headerMode: 'none',
    }

    constructor(props) {
        super(props)
        this.state = {
            nik: null,
            pass: null,
            token: null,
            loading: false,
            ip: null
        },
        AsyncStorage.getItem('token', (error, data) => {
            this.setState({token: data})
        })
    }

    componentWillMount() {
        Axios.get(urlApi+'/ip').then(response => {
            this.setState({ip: response.data.ip})
        });
    }

    getDateNow(date) {
        var now = new Date(date),
            month = '' + (now.getMonth() + 1),
            day = '' + (now.getDate()),
            year = now.getFullYear();
        if (month.length < 2) month = '0' + month
        if (day.length < 2) day = '0' + day

        return [year, month, day].join('/');
    }

    getTimeNow(date) {
        let now = new Date(date),
            hh = now.getHours(),
            mm = now.getMinutes(),
            ss = now.getSeconds();
        if(hh.length < 2) hh = '0' + hh
        if(mm.length < 2) mm = '0' + mm
        if(ss.length < 2) ss = '0' + ss

        return [hh,mm,ss].join(':')
    }
    
    loginAction = () => {
        const storageNik = this.state.nik
        const navigate = this.props.navigation
        const nikAtauPassSalah = this.refs.peringatan.show(<View><Text style={{color: '#ff0000'}}>NIK atau Password Salah !</Text></View>)
        const toastPeringatan = this.refs.peringatan
        console.log(this.state.nik)
        // console.log(urlApi+'/action/login')
        if(this.state.nik == null || this.state.pass == null) {
            this.refs.peringatan.show(<View><Text style={{color: '#ff0000'}}>Harap Diisi NIK dan Password</Text></View>)
        } else {
            this.setState({loading: true})
            Axios.post(urlApi+'/action/login', {
                nik: this.state.nik,
                password: this.state.pass
            }).then(response => {
                if(response.status != 200) {
                    // nikAtauPassSalah
                    this.setState({loading: false})
                } else {
                    AsyncStorage.setItem('token', response.data)
                    AsyncStorage.setItem('nik', storageNik)
                    this.getEmployee(this.state.nik)
                        .then(data => {
                            let employeeData = {
                                name: data.name,
                                division: data.division,
                                position: data.position,
                                photo: data.photo,
                                expired: data.expired,
                                ip: this.state.ip
                            }
                            AsyncStorage.setItem('dataEmployee', JSON.stringify(employeeData))
                            this.doWriteActivity()
                            navigate.dispatch(StackActions.push({routeName: 'Home'}))
                            this.setState({loading: false})
                        }).catch(error => {
                            console.log(error)
                        })
                }
            }).catch(error => {
                toastPeringatan.show(<View><Text style={{color: '#ff0000'}}>Something Error</Text></View>)
                this.setState({loading: false})
            })
        }
    }

    doWriteActivity() {
        Axios.post(urlApi+'/activity/'+this.state.nik, {
            activity: 'Logging Application',
            tanggal: this.getDateNow(Date.now()),
            time : this.getTimeNow(Date.now()),
            ip: this.state.ip,
            employee_nik: this.state.nik
        }).then(response => {
            console.log(response.data)
        }).catch(error => {
            console.log(error)
        })
    }

    getEmployee(nik) {
        return Axios.get(urlApi+'/employee/'+nik)
            .then(res => {
                return res.data
            })
    }

    render() {
        return(
            <Container>
                <Overlay childrenWrapperStyle={{backgroundColor: 'rgba(0, 0, 0, 0)'}} containerStyle={{backgroundColor: 'rgba(52, 52, 52, 0.8)'}} visible={this.state.loading} onClose={() => this.setState({loading: false})}>
                    <Spinner color='green' />
                </Overlay>
                <Toast 
                    ref='peringatan' 
                    style={{backgroundColor: '#fff'}}
                />
                <ImageBackground style={styling.bgImage} source={require('./assets/bg-login.png')}>
                    <View style={styling.overlay} />
                    <Content>
                        <Grid>
                            <Row>
                                <Col style={{flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 80}}>
                                    <Image source={require('./assets/telkomsel.png')} style={{width: 150, height: 150}} />
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Item regular style={{marginLeft: 20, marginRight: 20, marginTop: 20, marginBottom: 20, justifyContent: 'center', alignItems: 'center'}}>
                                        <Icon name="person" style={{color: '#fff'}}/>
                                        <Input placeholderTextColor={'#fff'} placeholder="NIK" style={{color: '#fff'}} onChangeText={(nikInput) => this.setState({nik: nikInput})}/>
                                    </Item>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Item regular style={{marginLeft: 20, marginRight: 20, marginTop: 20, marginBottom: 20, justifyContent: 'center', alignItems: 'center'}}>
                                        <Icon name="key" style={{color: '#fff'}}/>
                                        <Input secureTextEntry={true} placeholderTextColor={'#fff'} placeholder="Password" onChangeText={(passInput) => this.setState({pass: passInput})}/>
                                    </Item>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Button onPress={this.loginAction} style={{marginLeft: 20, marginRight: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff', borderRadius: 5, width: '90%'}}><Text style={{color: '#ff0000'}}>LOGIN</Text></Button>
                                </Col>
                            </Row>
                        </Grid>
                    </Content>
                    <Text style={{justifyContent: 'center', textAlign: 'center', color: '#fff', fontSize: 15}}>{'\u00A9'} IT Support Jawa Barat {'\u00A9'}</Text>
                </ImageBackground>
            </Container>
        )
    }
}