import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity , AsyncStorage, Linking} from 'react-native';
import { Container, Content, Card, CardItem, Body } from 'native-base';
import { createStackNavigator, createAppContainer, StackActions, NavigationActions } from 'react-navigation';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Communications from 'react-native-communications';

export default class Contact extends React.Component {
    static navigationOptions = {
      title: 'Contact',
      headerStyle: {
        backgroundColor: '#FF0000',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        color: '#fff',
        textAlign: 'center',
        alignSelf: 'center'
      }
    }

    _callITSupport() {
      Communications.phonecall('0811720505' ,true)
    }

    _smsITSupport() {
      Communications.text('0811720505')
    }

    _waITSupport() {
      Linking.openURL('https://wa.me/6281221544539/?text=Test')
    }

    render() {
      return (
        <Container>
          <Content>
            <Grid>
              <Row>
                <Col style={{margin: 20}}>
                  <TouchableOpacity onPress={this._callITSupport}>
                      <Card style={{paddingTop: 30, paddingBottom: 30, borderRadius: 10}}>
                          <CardItem>
                              <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                                <Image source={require('./assets/call.png')}/>
                                <Text>Call IT Support</Text>
                              </Body>
                          </CardItem>
                      </Card>
                  </TouchableOpacity>
                </Col>
                <Col style={{margin: 20}}>
                  <TouchableOpacity onPress={this._smsITSupport}>
                      <Card style={{paddingTop: 30, paddingBottom: 30, borderRadius: 10}}>
                          <CardItem>
                              <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                                  <Image source={require('./assets/sms.png')}/>
                                  <Text>SMS IT Support</Text>
                              </Body>
                          </CardItem>
                      </Card>
                  </TouchableOpacity>
                </Col>
              </Row>
              <Row>
                <Col style={{margin: 20}}>
                  <TouchableOpacity onPress={this._waITSupport}>
                    <Card style={{paddingTop: 30, paddingBottom: 30, borderRadius: 10}}>
                        <CardItem>
                            <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                                <Image source={require('./assets/wa.png')}/>
                                <Text>Whatsapp IT Support</Text>
                            </Body>
                        </CardItem>
                    </Card>
                  </TouchableOpacity>
                </Col>
                <Col style={{margin: 20}}>
                  <TouchableOpacity onPress={this.telegramITSupport}>
                    <Card style={{paddingTop: 30, paddingBottom: 30, borderRadius: 10}}>
                      <CardItem>
                        <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                          <Image source={require('./assets/telegram.png')} />
                          <Text>Telegram IT Support</Text>
                        </Body>
                      </CardItem>
                    </Card>
                  </TouchableOpacity>
                </Col>
              </Row>
            </Grid>
          </Content>
        </Container>
      )
    }
  }