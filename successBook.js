import React from 'react'
import { 
    Text, 
    Image, 
    View, 
    StyleSheet 
} from 'react-native'
import { Container, Row, Button } from 'native-base';
import { Grid } from 'react-native-easy-grid';
import { 
    StackActions,
    NavigationActions
} from 'react-navigation';

export default class successBook extends React.Component {
    static navigationOptions = {
        title: 'Booking',
        headerStyle: {
          backgroundColor: '#FF0000',
        },
        headerTitleStyle: {
          color: '#fff',
          textAlign: 'center',
          alignSelf: 'center'
        }
    }

    render() {
        return(
            <Container style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Grid>
                    <Row style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                        <Image source={require('./assets/checked.png')} />
                    </Row>
                    <Row style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                        <Button style={{width: '60%'}} danger onPress={() => this.props.navigation.dispatch(StackActions.reset({index: 0, actions: [NavigationActions.navigate({routeName: 'Home'})]}))}><Text style={{flex: 1,justifyContent: 'center', alignItems: 'center', color: '#fff'}}>Return To Home</Text></Button>
                    </Row>
                </Grid>
            </Container>
        )
    }
}
