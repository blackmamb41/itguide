import React from 'react'
import {
    TouchableOpacity,
    View,
    Text,
    Image
} from 'react-native'
import {
    Card,
    CardItem,
    Body,
    Container,
    Content
} from 'native-base'
import { 
    StackActions, 
    NavigationActions 
} from 'react-navigation';
import { Grid, Row, Col } from 'react-native-easy-grid';

export default class Tutorial extends React.Component {
    static navigationOptions = {
        title: 'Tutorial',
        headerStyle: {
          backgroundColor: '#FF0000',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          color: '#fff',
          textAlign: 'center',
          alignSelf: 'center'
        }
    }

    render() {
        return(
            <Container>
                <Content>
                    <Grid>
                        <Row>
                            <Col style={{margin: 20}}>
                                <TouchableOpacity onPress={this.goToBookingCalendar}>
                                    <Card style={{paddingTop: 30, paddingBottom: 30, borderRadius: 10}}>
                                        <CardItem>
                                            <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                                                <Image source={require('./assets/phone.png')} />
                                                <Text>How To Booking</Text>
                                            </Body>
                                        </CardItem>
                                    </Card>
                                </TouchableOpacity>
                            </Col>
                            <Col style={{margin: 20}}>
                                <TouchableOpacity onPress={this.goToTutorial}>
                                    <Card style={{paddingTop: 30, paddingBottom: 30, borderRadius: 10}}>
                                        <CardItem>
                                            <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                                                <Image source={require('./assets/avaya.png')} />
                                                <Text>Avaya Conference</Text>
                                            </Body>
                                        </CardItem>
                                    </Card>
                                </TouchableOpacity>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={{margin: 20}}>
                                <TouchableOpacity onPress={this.goToBookingCalendar}>
                                    <Card style={{paddingTop: 30, paddingBottom: 30, borderRadius: 10}}>
                                        <CardItem>
                                            <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                                                <Image source={require('./assets/siaga.png')} style={{width: 128, height: 64}}/>
                                                <Text>Link Aplikasi Siaga</Text>
                                            </Body>
                                        </CardItem>
                                    </Card>
                                </TouchableOpacity>
                            </Col>
                            <Col style={{margin: 20}}>
                                <TouchableOpacity onPress={this.goToTutorial}>
                                    <Card style={{paddingTop: 30, paddingBottom: 30, borderRadius: 10}}>
                                        <CardItem>
                                            <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                                                <Image source={require('./assets/massage.png')} />
                                                <Text>Message Chair</Text>
                                            </Body>
                                        </CardItem>
                                    </Card>
                                </TouchableOpacity>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={{margin: 20}}>
                                <TouchableOpacity onPress={this.goToBookingCalendar}>
                                    <Card style={{paddingTop: 30, paddingBottom: 30, borderRadius: 10}}>
                                        <CardItem>
                                            <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                                                <Image source={require('./assets/matrix.png')}/>
                                                <Text>Matrix TV</Text>
                                            </Body>
                                        </CardItem>
                                    </Card>
                                </TouchableOpacity>
                            </Col>
                            <Col style={{margin: 20}}>
                                <TouchableOpacity onPress={this.goToTutorial}>
                                    <Card style={{paddingTop: 30, paddingBottom: 30, borderRadius: 10}}>
                                        <CardItem>
                                            <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                                                <Image source={require('./assets/wifi.png')} />
                                                <Text>Connect Wifi</Text>
                                            </Body>
                                        </CardItem>
                                    </Card>
                                </TouchableOpacity>
                            </Col>
                        </Row>
                    </Grid>
                </Content>
            </Container>
        )
    }
}