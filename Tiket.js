import React from 'react'
import { 
    Text, 
    AsyncStorage, 
    Image,
    View,
    TouchableOpacity,
} from 'react-native';
import { 
    Container, 
    Content, 
    Badge, 
    Card, 
    CardItem, 
    Body,
    Spinner,
    Icon,
    Toast,
    Button
} from 'native-base';
import Axios from 'axios';
import { urlApi } from './utils';
import Modal from "react-native-modal";
import { Grid, Row, Col } from 'react-native-easy-grid';
import { unserialize } from 'locutus/php/var';

export default class Tiket extends React.Component {
    static navigationOptions = {
        title: 'Tiket',
        headerStyle: {
          backgroundColor: '#FF0000',
          color: '#fff'
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          color: '#fff',
          textAlign: 'center',
          alignSelf: 'center'
        }
    }

    constructor(props) {
        super(props)
        this.state = {
            nik: null,
            tiket: null,
            data: [],
            loading: false,
            tiketModalVisible: false,
            singleTiket: {}
        }
    }

    async componentDidMount() {
        let valueNik = await AsyncStorage.getItem('nik')
        this.setState({nik: valueNik})
        this._getDataHistory()
        console.log(this.state.nik)
    }

    _getDataHistory() {
        this.setState({loading: !this.state.loading})
        Axios.get(urlApi+'/employee/'+this.state.nik+'/history')
            .then(response => {
                this.setState({data: response.data})
                this.setState({loading: !this.state.loading})
            }).catch(error => {
                console.log(error)
            })
    }

    _onPressSingleTiket = (id) => {
        Axios.get(urlApi+'/tiket/show/'+id)
            .then(response => {
                // console.log(response.data)
                this.setState({
                    tiketModalVisible: !this.state.tiketModalVisible,
                    singleTiket: response.data
                })
                AsyncStorage.setItem('dataTiket', JSON.stringify(response.data))
                this.props.navigation.navigate('Solotiket', {title: response.data.tiket})
            }).catch(error => {
                console.log(error)
            })
    }

    renderTiket() {
        if(this.state.loading == true) {
            return(
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                    <Spinner color='green' />
                </View>
            )
        } else {
            if(this.state.data.length == 0) {
                return(<Text>Tidak Ada Tiket</Text>)
            } else {
                const tiket = this.state.data.map((item) => {
                    const header = <CardItem header><Image source={require('./assets/telkomsel.png')} style={{width: 20, height: 20}} /><Text> Tanggal : {item.tanggal}</Text></CardItem>
                    const body = <CardItem><Body><Text>{item.perihal}</Text></Body></CardItem>
                    if(item.status == 'checkin') {
                        return (
                            <TouchableOpacity onPress={() => this._onPressSingleTiket(item.id)}>
                                <Card style={{marginLeft: 20, marginRight: 20}}>
                                    {header}
                                    {body}
                                    <CardItem><Badge success><Text style={{color: '#fff'}}>Checkin</Text></Badge></CardItem>
                                </Card>
                            </TouchableOpacity>
                        )
                    } else if(item.status == 'pending') {
                        return (
                            <TouchableOpacity onPress={() => this._onPressSingleTiket(item.id)}>
                                <Card style={{marginLeft: 20, marginRight: 20}}>
                                    {header}
                                    {body}
                                    <CardItem><Badge warning><Text>Pending</Text></Badge></CardItem>
                                </Card>
                            </TouchableOpacity>
                        )
                    } else if(item.status == 'cancel') {
                        return (
                            <TouchableOpacity onPress={() => this._onPressSingleTiket(item.id)}>
                                <Card style={{marginLeft: 20, marginRight: 20}}>
                                    {header}
                                    {body}
                                    <CardItem><Badge danger><Text style={{color: '#fff'}}>Cancel</Text></Badge></CardItem>
                                </Card>
                            </TouchableOpacity>
                        )
                    }
                })
                return tiket
            }
        }
    }

    render() {
        return (
            <Container style={{flex: 1, justifyContent: 'center'}}>
                <Content>
                    {this.renderTiket()}
                </Content>
            </Container>
        )
    }
}