import React from 'react';
import { Text, Image, StyleSheet, View } from "react-native";
import AppIntroSlider from 'react-native-app-intro-slider';
import { StackActions, NavigationActions } from "react-navigation";
import { Icon } from 'native-base';

const styles = StyleSheet.create({
    image: {
      width: 120,
      height: 120,
    }
});

const AvayaData = [
    {
        key: '1',
        text: 'Lore Elipsum Done Lore Elipsum Done Lore Elipsum Done Lore Elipsum DoneLore Elipsum DoneLore Elipsum DoneLore Elipsum Done',
        image: require('./assets/tv.png'),
        imageStyle: styles.image,
        backgroundColor: '#000'
    },
    {
        key: '2',
        text: 'blank',
        image: require('./assets/blank.png'),
        imageStyle: styles.image,
    },
];

const homeNavigation = StackActions.reset({
    index: 0,
    actions: [
      NavigationActions.navigate({routeName: 'Home'})
    ],
})
export default class AvayaTutor extends React.Component {
    static navigationOptions = {
        mode: 'modal',
        header: null,
        headerMode: 'none',
    }

    _renderNextButton = () => {
        return (
          <View style={styles.buttonCircle}>
            <Icon
              name="arrow-forward"
              color="rgba(255, 255, 255, .9)"
              size={24}
              style={{ backgroundColor: 'transparent' }}
            />
          </View>
        );
    }

    _renderDoneButton = () => {
        return (
          <View style={styles.buttonCircle}>
            <Icon
              name="checkmark-circle"
              color="rgba(255, 255, 255, .9)"
              size={24}
              style={{ backgroundColor: 'transparent' }}
            />
          </View>
        );
    }

    _sudahSelesai = () => {
        this.props.navigation.dispatch(homeNavigation);
    }
    
    render() {
        return (
            <AppIntroSlider 
                slides={AvayaData}
                onDone={this._sudahSelesai}
                renderNextButton={this._renderNextButton}
                renderDoneButton={this._renderDoneButton}
            />
        )
    }
}