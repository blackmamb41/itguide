import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, AsyncStorage, ImageBackground } from 'react-native';
import { Container, Content, Footer, FooterTab, Button, Header, Icon, DeckSwiper, Badge, Card, CardItem, Body } from 'native-base';
import { createStackNavigator, createAppContainer, StackActions, NavigationActions } from 'react-navigation';
import { Col, Grid, Row } from 'react-native-easy-grid';
// import { goToBookingCalendar } from "./Providers/bookingFunction";

const Layout = StyleSheet.create({
  mainMenu: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    margin: 20,
    paddingTop: 30,
    paddingBottom: 30,
    // paddingLeft: 50,
    // paddingRight: 50,
    // borderWidth: 0.5,
    // borderRadius: 9,
    // borderColor: '#ddd',
    // borderBottomWidth: 0,
    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 2 },
    // shadowOpacity: 0.8,
    // shadowRadius: 9,
    // elevation: 3,
  },
  tutorialContain: {
    flex: 1,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 20,
    marginBottom: 20,
    paddingTop: 15,
    paddingBottom: 15,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: '#ddd',
    borderRadius: 9,
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 9,
    elevation: 3,
  },
  titleMenu: {
    textAlign: 'center',
    fontSize: 10,
  },
  thumbMenu: {
    flex: 1,
    alignItems: 'center',
    width: 70,
    height: 70,
  },
  bookingButton: {
    marginLeft: 120,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
    width: 130,
    height: 40,
    backgroundColor: '#ff0000',
    color: '#fff',
    borderRadius: 8,
    elevation: 5,
  }
})

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    // title: 'Home',
    // headerStyle: {
    //   backgroundColor: '#FF0000',
    // },
    // headerTitleStyle: {
    //   color: '#fff',
    //   textAlign: 'center',
    //   flex: 1,
    // }
    mode: 'modal',
    header: null,
    headerMode: 'none',
  }

  constructor(props) {
    super(props)
    this.state = {
      employee: {}
    }
  }

  componentWillMount() {
    AsyncStorage.getItem('token', (error, data) => {
      if(data == null) {
        this.props.navigation.navigate('Login')
      } else {
        // this.props.navigation.dispatch(StackActions.reset({index: 0, actions: [NavigationActions.navigate({routeName: 'Home'})]}))
        this.props.navigation.navigate('Home')
      }
    })
    AsyncStorage.getItem('dataEmployee', (error, data) => {
      jsonData = JSON.parse(data)
      this.setState({employee: jsonData})
    })
    console.log(this.state.employee)
  }

  goToBookingCalendar = () => {
    this.props.navigation.navigate('Booking')
  }
  
  goToTiket = () => {
    this.props.navigation.navigate('Tiket')
  }

  goToTutorial = () => {
    this.props.navigation.navigate('Tutorial')
  }

  goToContact = () => {
    this.props.navigation.navigate('Contact')
  }

  goToSettings = () => {
    this.props.navigation.navigate('Settings')
  }

  goToInformation = () => {
    this.props.navigation.navigate('Information')
  }

  profileCondition = () => {
    AsyncStorage.getItem('token', (error, data) => {
      if(data == null) {
        this.props.navigation.dispatch(StackActions.reset({index: 0, actions: [NavigationActions.navigate({routeName: 'Login'})]}))
      } else {
        this.props.navigation.dispatch(StackActions.reset({index: 0, actions: [NavigationActions.navigate({routeName: 'Profile'})]}))
      }
    })
  }

  HistoryCondition = () => {
    AsyncStorage.getItem('token', (error, data) => {
      if(data == null) {
        this.props.navigation.dispatch(StackActions.reset({index: 0, actions: [NavigationActions.navigate({routeName: 'Login'})]}))
      } else {
        this.props.navigation.dispatch(StackActions.reset({index: 0, actions: [NavigationActions.navigate({routeName: 'History'})]}))
      }
    })
  }

  BookingCondition = () => {
    AsyncStorage.getItem('token', (error, data) => {
      if(data == null) {
        this.props.navigation.dispatch(StackActions.reset({index: 0, actions: [NavigationActions.navigate({routeName: 'Login'})]}))
      } else {
        // this.props.navigation.dispatch(StackActions.reset({index: 0, actions: [NavigationActions.navigate({routeName: 'Booking'})]}))
        this.props.navigation.navigate('Booking')
      }
    })
  }

  render() {
    return (
      <Container>
        <Grid>
          <Row>
              <ImageBackground style={{flex: 1, width: '100%', height: '100%'}} source={require('./assets/bg-login.png')}>
                <Col style={{flex: 1, justifyContent: 'flex-start', alignItems: 'center', marginTop: 50}}>
                  <Image style={{borderWidth: 2, borderColor: '#fff', borderRadius: 50, backgroundColor: '#fff'}} source={require('./assets/user.png')} />
                  <Text style={{color: '#fff', fontWeight: 'bold', margin: 5, marginTop: 10}}>{this.state.employee.name}</Text>
                  <Text style={{color: '#fff', fontWeight: 'bold', margin: 5}}>{this.state.employee.position} - {this.state.employee.division}</Text>
                </Col>
              </ImageBackground>
          </Row>
          <Row>
            <Col style={{margin: 20}}>
              <TouchableOpacity onPress={this.goToBookingCalendar}>
                <Card style={{paddingTop: 30, paddingBottom: 30, borderRadius: 10}}>
                  <CardItem>
                    <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                      <Image source={require('./assets/menu/clipboard.png')} />
                      <Text>Booking</Text>
                    </Body>
                  </CardItem>
                </Card>
              </TouchableOpacity>
            </Col>
            <Col style={{margin: 20}}>
              <TouchableOpacity onPress={this.goToTutorial}>
                <Card style={{paddingTop: 30, paddingBottom: 30, borderRadius: 10}}>
                  <CardItem>
                    <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                      <Image source={require('./assets/menu/tutorial.png')} />
                      <Text>Tutorial</Text>
                    </Body>
                  </CardItem>
                </Card>
              </TouchableOpacity>
            </Col>
          </Row>
          <Row>
            <Col style={{margin: 20}}>
              <TouchableOpacity onPress={this.goToTiket}>
                <Card style={{paddingTop: 30, paddingBottom: 30, borderRadius: 10}}>
                  <CardItem>
                    <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                      <Image source={require('./assets/menu/invoice.png')} />
                      <Text>Tiket</Text>
                    </Body>
                  </CardItem>
                </Card>
              </TouchableOpacity>
            </Col>
            <Col style={{margin: 20}}>
              <TouchableOpacity onPress={this.goToInformation}>
                <Card style={{paddingTop: 30, paddingBottom: 30, borderRadius: 10}}>
                  <CardItem>
                    <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                      <Image source={require('./assets/menu/calendar.png')} />
                      <Text>Information</Text>
                    </Body>
                  </CardItem>
                </Card>
              </TouchableOpacity>
            </Col>
          </Row>
          <Row>
            <Col style={{margin: 20}}>
              <TouchableOpacity onPress={this.goToContact}>
                <Card style={{paddingTop: 30, paddingBottom: 30, borderRadius: 10}}>
                  <CardItem>
                    <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                      <Image source={require('./assets/menu/operator.png')} />
                      <Text>Contact</Text>
                    </Body>
                  </CardItem>
                </Card>
              </TouchableOpacity>
            </Col>
            <Col style={{margin: 20}}>
              <TouchableOpacity onPress={this.goToSettings}>
                <Card style={{paddingTop: 30, paddingBottom: 30, borderRadius: 10}}>
                  <CardItem>
                    <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                      <Image source={require('./assets/menu/settings.png')} />
                      <Text>Settings</Text>
                    </Body>
                  </CardItem>
                </Card>
              </TouchableOpacity>
            </Col>
          </Row>
        </Grid>
      </Container>
    )
  }
}