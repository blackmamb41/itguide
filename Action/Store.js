import { createStore, combineReducers } from 'redux';
import loginReducer from "./Reducer";

const rootReducer = combineReducers({
    login: loginReducer
})

const configureStore = () => {
    return createStore(rootReducer);
}

export default configureStore;