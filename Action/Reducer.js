import { LOGIN, LOGOUT } from './Types'

const initialState = {
    credentials: ''
}

const loginReducer = (state = initialState, action) => {
    switch(action.type) {
        case "LOGIN":
            return {
                ...state,
                payload: action.payload
            }
        default:
            return state;
    }
}

export default loginReducer