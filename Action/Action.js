import { LOGIN, LOGOUT } from './Types'

export const login = credentials => {
    return {
        type: LOGIN,
        payload: credentials
    }
}