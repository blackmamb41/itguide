import React from 'react'
import { 
    Text, 
    View,
    AsyncStorage,
    Image
} from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import { Container, Button } from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { unserialize } from 'locutus/php/var';
import Axios from 'axios';
import { urlApi } from './utils';
import SectionedMultiSelect from "react-native-sectioned-multi-select";

export default class Solotiket extends React.Component {
    static navigationOptions = ({navigation}) => ({
        title: 'Tiket Nomor ' + navigation.state.params.title,
        headerStyle: {
          backgroundColor: '#FF0000',
          color: '#fff'
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          color: '#fff',
          textAlign: 'center',
          alignSelf: 'center'
        }
    })

    constructor(props) {
        super(props)
        this.state = {
            tiketJson: {},
            audience: [],
        }
    }

    async componentWillMount() {
        nomorTiket = null
        let data = await AsyncStorage.getItem('dataTiket')
        this.setState({tiketJson: JSON.parse(data)})
        // getAudience()
    }

    getAudience() {
        Axios.get(urlApi+'/employee')
            .then(response => {
                this.setState({audience: response.data})
                console.log(response.data)
            }).catch(error => {
                console.log(error)
            })
        console.log(this.state.audience)
    }

    jamToString() {
        const jam = unserialize(this.state.tiketJson.jam).toString()
        return jam
    }

    _doCancelBooking = () => {
        Axios.post(urlApi+'/booking/cancel/'+this.state.tiketJson.id)
            .then(response => {
                console.log(response.data)
                this.props.navigation.dispatch(StackActions.push({routeName: 'Tiket'}))
            }).catch(error => {
                console.log(error)
            })
    }

    render() {
        return (
            <Container>
                <Grid>
                    <Row style={{height: '25%'}}>
                        <Col style={{width: '40%'}}>
                            <Image source={require('./assets/telkomsel.png')} style={{width: 120, height: 120, margin: 20}}/>
                        </Col>
                        <Col style={{marginTop: 20}}>
                            <Text style={{fontSize: 15, fontWeight: 'bold'}}>PIC : {this.state.tiketJson.employees_nik}{"\n"}</Text>
                            <Text style={{fontSize: 15, fontWeight: 'bold'}}>Tanggal : {this.state.tiketJson.tanggal}{"\n"}</Text>
                            <Text style={{fontSize: 15, fontWeight: 'bold'}}>Tempat : WARROM - REGIONAL JABAR{"\n"}</Text>
                            <Text style={{fontSize: 15, fontWeight: 'bold'}}>JAM : {"\n"}</Text>
                        </Col>
                    </Row>
                    <View style={{borderBottomWidth: 1, borderBottomColor: 'grey', justifyContent: 'flex-start', marginRight: 10, marginLeft: 10}} />
                    <Text style={{margin: 20}}>PERIHAL : {"\n"}</Text>
                    <Text style={{marginLeft: 20, marginRight: 20, marginTop: 10}}>{this.state.tiketJson.perihal}</Text>
                    <Row>
                        <Col style={{flex: 1, flexDirection: 'column-reverse', justifyContent: 'flex-start', alignItems: 'flex-end', margin: 20}}>
                            <Button style={{width: '100%', justifyContent: 'center', alignItems: 'center'}} danger onPress={this._doCancelBooking}><Text style={{color: '#fff'}}>CANCEL BOOKING</Text></Button>
                            <Button style={{width: '100%', justifyContent: 'center', alignItems: 'center'}} primary onPress={this._showAudienceList}><Text style={{color: '#fff'}}>AUDIENCE</Text></Button>
                        </Col>
                    </Row>
                </Grid>
            </Container>
        )
    }
}