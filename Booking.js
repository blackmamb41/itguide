import React from "react";
import { 
    StyleSheet,
    View, 
    Text, 
    FlatList, 
    AsyncStorage,
    TouchableOpacity
} from "react-native";
import { 
    Button, 
    Container, 
    Content, 
    Footer, 
    FooterTab, 
    Icon, 
    Item, 
    Input, 
    Spinner, 
    List, 
    Form, 
    Textarea,
    Badge
} from "native-base";
import { Font, AppLoading } from "expo"
import Axios from "axios";
import { Grid, Col, Row } from "react-native-easy-grid";
import { urlApi } from "./utils"
import Modal from "react-native-modal";
import Toast, {DURATION} from 'react-native-easy-toast'
import DatePicker from 'react-native-datepicker'
import { SelectMultipleButton, SelectMultipleGroupButton } from 'react-native-selectmultiple-button';
import { unserialize } from "locutus/php/var";

const styling = StyleSheet.create({
    buttonTanggal: {
        marginRight: 80,
        marginLeft: 80,
        margin: 10, 
        width: '30%',
        flex: 1, 
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'center',
        backgroundColor: '#ff0000'
    },
    buttonCheck: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'green',
        marginRight: 140,
        marginLeft: 140,
    },
    buttonJam: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 20,
        width: '20%',
        borderRadius: 50
    },
    buttonBookNow: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10,
        marginRight: 50,
        height: '10%',
        width: '100%',
        borderRadius: 5,
        backgroundColor: '#ff0000',
    }
})

const defaultClock = [
    { value: 7, displayValue: 7},
    { value: 8, displayValue: 8},
    { value: 9, displayValue: 9},
    { value: 10, displayValue: 10},
    { value: 11, displayValue: 11},
    { value: 12, displayValue: 12},
    { value: 13, displayValue: 13},
    { value: 14, displayValue: 14},
    { value: 15, displayValue: 15},
    { value: 16, displayValue: 16},
    { value: 17, displayValue: 17},
    { value: 18, displayValue: 18},
]
const dataset = [7,8,9,10,11,12,13,14,15,16,17]
const indexJam = []

class Clockbutton extends React.Component {
    render() {
        return (
            <SelectMultipleGroupButton
                defaultSelectedIndexes={indexJam}
                multiple={true}
                containerViewStyle={{ flexWrap: 'wrap', flexDirection: 'row', justifyContent: 'center' }}
                highLightStyle={{
                    borderColor: "gray",
                    backgroundColor: "transparent",
                    textColor: "gray",
                    borderTintColor: "green",
                    backgroundTintColor: "green",
                    textTintColor: "white"
                }}
                buttonViewStyle={{ width: 80, height: 80, borderRadius: 40 }}
                onSelectedValuesChange={selected => this.newCheckBooking(selected)}
                group={defaultClock}
            />
        )
    }
}

export default class Booking extends React.Component {
    static navigationOptions = {
        title: 'Booking',
        headerStyle: {
          backgroundColor: '#FF0000',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          color: '#fff',
          textAlign: 'center',
          alignSelf: 'center'
        }
    }

    constructor(props) {
        super(props);
        this.state = { 
            tanggal: null,
            loading: true,
            intialClock: [
                { jam: 6, status: 'empty'},
                { jam: 7, status: 'empty'},
                { jam: 8, status: 'empty'},
                { jam: 9, status: 'empty'},
                { jam: 10, status: 'empty'},
                { jam: 11, status: 'empty'},
                { jam: 12, status: 'empty'},
                { jam: 13, status: 'empty'},
                { jam: 14, status: 'empty'},
                { jam: 15, status: 'empty'},
                { jam: 16, status: 'empty'},
                { jam: 17, status: 'empty'},
                { jam: 18, status: 'empty'},
            ],
            dataJam: [],
            modalCreateVisible: false,
            modalGetSingleVisible: false,
            // pilihJam: null,
            pilihJam: [],
            perihal: null,
            dataEmployee: {},
            nikEmployee: null,
            singleData: {},
            statusDone: false,
            toggle: true
        };
        this.setDate = this.setDate.bind(this);
        this._toggleModalCreateBooking = this._toggleModalCreateBooking.bind(this)
    }

    async componentDidMount() {
        await AsyncStorage.getItem('dataEmployee', (error, data) => {
            jsonData = JSON.parse(data)
            this.setState({dataEmployee: jsonData})
        })
        await AsyncStorage.getItem('nik', (error, data) => {
            this.setState({nikEmployee: data})
        })
        // console.log(this.state.dataEmployee)
    }

    getDateNow(date) {
        var now = new Date(date),
            month = '' + (now.getMonth() + 1),
            day = '' + (now.getDate()),
            year = now.getFullYear();
        if (month.length < 2) month = '0' + month
        if (day.length < 2) day = '0' + day

        return [year, month, day].join('/');
    }

    getTimeNow(date) {
        let now = new Date(date),
            hh = '' + (now.getHours()),
            mm = '' + (now.getMinutes());
        if(hh.length < 2) hh = '0' + hh
        if(mm.length < 2) mm = '0' + mm

        return [hh,mm].join(':');
    }

    setDate(tglDipilih) {
        this.setState({tanggal: tglDipilih})
        this.newMethodForCheckBooking()
        // console.log(this.state.tanggal)
        console.log(this.state.dataEmployee)
        // this.refs.peringatan.show(<View><Text style={{color: '#fff', justifyContent: 'center', alignItems: 'center'}}>HARAP DITEKAN TOMBOL SEARCH DAHULU SEBELUM MELAKUKAN BOOKING {"\n"}</Text><Icon style={{color: '#fff'}} name='search'></Icon></View>)
    }

    async componentWillMount() {
        await Font.loadAsync({
          Roboto: require("native-base/Fonts/Roboto.ttf"),
          Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
        });
        this.setState({ loading: false });
    }

    getDataCheckBooking = () => {
        Axios.get(urlApi+'/booking/check/tanggal/'+this.state.tanggal.toString().substr(4, 12))
            .then(response => {
                this.setState({dataJam: response.data.data})
            }).catch(function(err) {
                console.log(err)
            })
    }

    _toggleModalCreateBooking = (jam) => {
        this.setState({ pilihJam: jam })
        this.setState({ modalCreateVisible: !this.state.modalCreateVisible });
        // console.log(jam, this.state.perihal)
    }

    _getSingleBooking = (jam) => {
        // UNUSED FUNCTION
        this.setState({ 
            pilihJam: jam,
            modalGetSingleVisible: !this.state.modalGetSingleVisible
        })
        fetch(urlApi+'/booking/check/jam/'+this.state.pilihJam+'/tanggal/'+this.state.tanggal.toString().substr(4,12)+'/user/'+this.state.nikEmployee)
            .then((response) => {
                console.log(response)
            }).catch((error) => {
                console.log(error)
            })
    }

    jamProccess() {
        const jam = this.state.intialClock.map((listJam) => {
            return listJam.jam
        })
        return jam
    }

    jamButton() {
        const jam = [7,8,9,10,11,12,13,14,15,16,17,18]
        const bookedJam = this.state.dataJam.map((bookingJam) => {
            return bookingJam.jam
        })
        const booked = this.state.dataJam.map((booking) => {            
            let newJam = {
                jam: booking.jam,
                status: booking.status
            }
            return newJam
        })
        const filter = jam.filter(function(list) {
            return !bookedJam.includes(list)
        })
        const excluded = filter.map((exJam) => {
            let jamKosong = {
                jam: exJam,
                status: 'empty'
            }
            return jamKosong
        })
        const margeJam = booked.concat(excluded)
        function compare(a,b) {
            if(a.jam < b.jam)
                return -1;
            if(a.jam > b.jam)
                return 1;
            return 0
        }
        margeJam.sort(compare);
        if(this.state.tanggal == null) {
            return(<Text>Pilih Tanggal dulu Coy</Text>)
        } else {
            return(
                <FlatList
                    data={margeJam}
                    renderItem={({item}) => {
                        if(item.status == 'empty') {
                            return <Button style={styling.buttonJam} light onPress={() => this._toggleModalCreateBooking(item.jam)}><Text>{item.jam}</Text></Button>
                        } else if(item.status == 'checkin') {
                            return <Button style={styling.buttonJam} success><Text>{item.jam}</Text></Button>
                        } else if(item.status == 'pending') {
                            return <Button style={styling.buttonJam} warning><Text>{item.jam}</Text></Button>
                        }
                    }}
                    numColumns={4}
                    keyExtractor={(item, index) => item.jam}
                />
            )
        }
    }

    createBooking = () => {
        if(this.state.perihal == null) {
            this.setState({ modalCreateVisible: !this.state.modalCreateVisible });
            this.refs.peringatan.show(<View><Text style={{color: '#fff', justifyContent: 'center', alignItems: 'center'}}>BOOKING DIBATALKAN{"\n"}{"\n"}DIKARENAKAN ANDA TIDAK MENGISI PERIHAL BOOKING</Text></View>)
        } else {
            Axios.post(urlApi+'/booking/add', {
                perihal: this.state.perihal,
                tanggal: this.state.tanggal.toString().substr(4, 12),
                jam: this.state.pilihJam,
                employees_nik: this.state.nikEmployee
            }).then((response) => {
                this.getDataCheckBooking()
                this.setState({ 
                    perihal: null,
                    modalCreateVisible: !this.state.modalCreateVisible
                });
            }).catch((error) => {
                console.log(error)
            })
        }
    }

    _closeModal = () => {
        this.setState({ 
            perihal: null,
            modalCreateVisible: !this.state.modalCreateVisible 
        });
    }

    newCheckBooking(selected) {
        this.setState({pilihJam: selected})
        console.log(this.state.pilihJam)
    }

    newMethodForCheckBooking() {
        Axios.get(urlApi+'/booking/check/tanggal/'+this.state.tanggal)
            .then(response => {
                let margeJam = []
                let jamBooked = response.data.data.map(data => {
                    unserialize(data.jam).map(datajam => {
                        margeJam.push(datajam)
                    })
                })
                this.setState({pilihJam: margeJam})
                let resultIndex = this.state.pilihJam.map(data => {
                    find = dataset.indexOf(data)
                    return find
                })
                resultIndex.map(dataResult => {
                    indexJam.push(dataResult)
                })
                this.setState({statusDone: true})
                // console.log(indexJam)
            }).catch(error => {
                console.log(error)
            })
        // console.log(this.state.dataEmployee)
    }
    refreshButton = () => {
        this.newMethodForCheckBooking()
    }

    presSwitch(){
        const stateSwitch = !this.state.toggle
        this.setState({toggle: stateSwitch})
        console.log(this.state.toggle)
    }

    renderJamButton() {
        // if(this.state.tanggal == null) {
        //     return (<Text>Silahkan Pilih Tanggal Dahulu</Text>)
        // } else if(this.state.statusDone == false) {
        //     return (<Spinner color='green' />)
        // } else if(this.state.statusDone == true) {
        //     return <Clockbutton />
        // }
        const bgSwitch = this.state.toggle?"blue":"red"
        return(
            <FlatList
                data={this.state.intialClock}
                renderItem={({item}) => {
                    return <Button><Text>{item.jam}</Text></Button>
                }}
            />
        )
    }
 
    render() {
        if(this.state.loading) {
            return(
                <AppLoading />
            )
        }
        return(
            <Container style={{flex: 1, justifyContent: 'center'}}>
                <Toast  
                    ref='peringatan'
                    style={{backgroundColor: '#ff0000', paddingTop: 150, paddingBottom: 150}}
                    position='top'
                    fadeOutDuration={4000}
                />
                <Content>
                    <Grid>
                        <Row style={{margin: 20, marginBottom: 100, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                            <DatePicker
                                    style={{width: 200}}
                                    date={this.state.tanggal} 
                                    mode='date'
                                    placeholder='Pilih Tanggal'
                                    format='YYYY-MM-DD'
                                    minDate={this.getDateNow(Date.now)}
                                    maxDate='2099-10-10'
                                    confirmBtnText='Pilih'
                                    onDateChange={this.setDate}
                                    customStyles={{
                                        dateInput: {
                                            borderRadius: 5,
                                        },
                                    }}
                                />
                        </Row>
                        {/* <Row>
                            <Col style={{flex: 1, flexDirection: 'row', alignContent: 'center', alignItems: 'center', justifyContent: 'center', width: '30%'}}>
                                <Button style={styling.buttonCheck} onPress={this.getDataCheckBooking}>
                                    <Icon name='search' />
                                </Button>
                            </Col>
                        </Row> */}
                        <Row style={{flex: 1, alignItems: 'center', justifyContent: 'center', margin: 10}}>
                            <Text>JAM :</Text>
                        </Row>
                        <Row>
                            {this.jamButton()}
                        </Row>
                        <Row>
                            <Text>Legend :{"\n"}</Text>
                        </Row>
                        <Row>
                            <Badge warning></Badge><Text>: Booked{"\n"}</Text>
                            <Badge sucess></Badge><Text>: Checkin{"\n"}</Text>
                            <Badge style={{backgroundColor: '#F6F6F6'}}></Badge><Text>: Empty</Text>
                        </Row>
                        <Row>
                            <Modal 
                                isVisible={this.state.modalGetSingleVisible}
                                deviceHeight={1}
                                transparent={false}
                            >
                                <Button onPress={() => this.setState({modalGetSingleVisible: !this.state.modalGetSingleVisible})}><Text>Close</Text></Button>
                            </Modal>
                            <Modal 
                                isVisible={this.state.modalCreateVisible}
                                deviceHeight={1}
                                transparent={false}
                            >
                                <TouchableOpacity onPress={this._closeModal}>
                                    <Icon name='close'></Icon>
                                </TouchableOpacity>
                                <Form style={{margin: 20, marginTop: 80}}>
                                    <Text>PERIHAL : {"\n"}</Text>
                                    <Textarea rowSpan={5} bordered onChangeText={(dataPerihal) => this.setState({perihal: dataPerihal})}>
                                    </Textarea>
                                </Form>
                                <Button style={styling.buttonBookNow} onPress={this.createBooking}><Text style={{color: '#fff', fontSize: 15, fontWeight: 'bold'}}>BOOK NOW</Text></Button>
                            </Modal>
                        </Row>
                    </Grid>
                </Content>
            </Container>
        )
        // return (
        //     <Container style={{flex: 1, alignItems: 'center'}}>
        //         <Content>
        //             <Grid>
        //                 <Row style={{margin: 20, marginBottom: 100, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
        //                     <DatePicker
        //                         style={{width: 200}}
        //                         date={this.state.tanggal} 
        //                         mode='date'
        //                         placeholder='Pilih Tanggal'
        //                         format='YYYY-MM-DD'
        //                         minDate={this.getDateNow(Date.now)}
        //                         maxDate='2099-10-10'
        //                         confirmBtnText='Pilih'
        //                         onDateChange={this.setDate}
        //                         customStyles={{
        //                             dateInput: {
        //                                 borderRadius: 5,
        //                             },
        //                         }}
        //                     />
        //                 </Row>
        //                 <Row>
        //                     <TouchableOpacity onPress={this.refreshButton}>
        //                         <Icon name="refresh-circle" />
        //                     </TouchableOpacity>
        //                 </Row>
        //                 <Row>
        //                     {this.jamButton()}
        //                 </Row>
        //                 <Row style={{marginTop: 40, flexDirection: 'row', justifyContent: 'center'}}>
        //                     <Button success style={{width: '50%', alignItems: 'center', justifyContent: 'center', borderRadius: 5}}><Text style={{color: '#fff'}}>Book Now</Text></Button>
        //                 </Row>
        //                 <Row>
        //                     <Text>{this.state.pilihJam}</Text>
        //                 </Row>
        //             </Grid>
        //         </Content>
        //     </Container>
        // )
    }
}