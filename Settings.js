import React from 'react'
import { 
    View, 
    TouchableOpacity,
    AsyncStorage
} from 'react-native';
import { 
    Container, 
    Content, 
    List, 
    ListItem, 
    Icon, 
    Text, 
    Left, 
    Body, 
    Right,
    Switch,
    Button,
    Spinner
} from 'native-base';
import { 
    StackActions, 
    NavigationActions 
} from 'react-navigation';
import Overlay from 'react-native-modal-overlay';

export default class Settings extends React.Component {
    static navigationOptions = {
        title: 'Settings',
        headerStyle: {
          backgroundColor: '#FF0000',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          color: '#fff',
          textAlign: 'center',
          alignSelf: 'center'
        }
    }

    constructor(props) {
        super(props)
        this.state = {
            notification: false,
            loadingLogout: false
        }
    }

    _doLogout = () => {
        this.setState({loadingLogout: true})
        AsyncStorage.removeItem('nik')
        AsyncStorage.removeItem('dataEmployee')
        AsyncStorage.removeItem('token')
        this.setState({loadingLogout: false})
        this.props.navigation.dispatch(StackActions.reset({index: 0, actions: [NavigationActions.navigate({routeName: 'Login'})]}))
    }

    closeLoading = () => {
        this.setState({loadingLogout: false})
    }

    render() {
        return(
            <Container>
                <Content>
                    <Overlay visible={this.state.loadingLogout} onClose={this.closeLoading}>
                        <Spinner color='red' />
                    </Overlay>
                    <ListItem icon>
                        <Left>
                            <Button style={{ backgroundColor: '#007AFF'}}>
                                <Icon name='megaphone' />
                            </Button>
                        </Left>
                        <Body>
                            <Text>Notification</Text>
                        </Body>
                        <Right>
                            <Switch value={false} />
                        </Right>
                    </ListItem>
                    <ListItem icon>
                        <Left>
                            <Button style={{ backgroundColor: '#E8A808'}}>
                                <Icon name='contrast' />
                            </Button>
                        </Left>
                        <Body>
                            <Text>Lamp</Text>
                        </Body>
                        <Right>
                            <Switch value={this.state.notification} />
                        </Right>
                    </ListItem>
                    <ListItem icon>
                        <Left>
                            <Button style={{ backgroundColor: '#696868'}}>
                                <Icon name='information-circle' />
                            </Button>
                        </Left>
                        <Body>
                            <TouchableOpacity>
                                <Text>
                                    About
                                </Text>
                            </TouchableOpacity>
                        </Body>
                    </ListItem>
                    <ListItem icon>
                        <Left>
                            <Button style={{ backgroundColor: '#ff0000'}}>
                                <Icon name='exit' />
                            </Button>
                        </Left>
                        <Body>
                            <TouchableOpacity onPress={this._doLogout}>
                                <Text>Logout</Text>
                            </TouchableOpacity>
                        </Body>
                    </ListItem>
                </Content>
            </Container>
        )
    }
}